<h1>Anilist Mod</h1>
<h2>About</h2>

This project aims to facilitate the sorting/viewing of anime saved on Anilist as well as simplify the filling of various challenges such as AWC.

Warning, the version presented here is not final and will improve with time.
The springboot part will be remove in the future to be more accessible to non-dev user.

<hr>

<h2>Summary</h2>

- [About](https://gitlab.com/SethNahu/anilistmods#about) 
- [Summary](https://gitlab.com/SethNahu/anilistmods#summary)
- [How to install](https://gitlab.com/SethNahu/anilistmods#how-to-install)
- [How to launch](https://gitlab.com/SethNahu/anilistmods#how-to-launch)
- [Usage](https://gitlab.com/SethNahu/anilistmods#usage)
    - [Search](https://gitlab.com/SethNahu/anilistmods#search)
        - [Filters](https://gitlab.com/SethNahu/anilistmods#search-filters)
        - [Result](https://gitlab.com/SethNahu/anilistmods#search-result)
    - [Random](https://gitlab.com/SethNahu/anilistmods#random)
        - [Filters](https://gitlab.com/SethNahu/anilistmods#random-filters)
        - [Result](https://gitlab.com/SethNahu/anilistmods#random-result)


<h2>How to install</h2>

1. Dowload the project on the download icon
![Download preview](https://i.imgur.com/jRevulP.png)
2. Then click on your preferred format 
<div align="center">
<img src="https://i.imgur.com/PfiqU6r.png" width="30%">
</div>
3. You now have the project installed on your computer

<h2>How to launch</h2>

1. Open your terminal
2. Go to the project folder
3. Enter the following line:
```
./gradlew bootRun
```

The project will launch itself.

When you see the following line, the project launch will be completed:
![launch picture](https://i.imgur.com/2fMOs4f.png)

The url is: http://localhost:8080/

<h2>Usage</h2>

<img src="https://i.imgur.com/K3m6q49.png" width="40%">

The application is divide into two tab: Search and Random.

<h3>Search</h3>

<h4>Search filters</h4>

The search row contains a lot of differents filters.

![search row](https://i.imgur.com/N3V6QTb.png)

- The ___username___ part is mandatory, as the project will get your own anime list.
- You can then select several ___status___ at the same time of your anime: Completed, Watching, Planned, Dropped etc.
- The ___Started Date___ will remove all the anime that you started before the given date.
- Same for the ___Finished Date___, it will remove all the anime that you finished before the given date.
- You can then select some ___tag___, ___genre___ or ___format___. If you search for a specific format/tag/genre, you can enter it in the design are.

<h4>Search result</h4>

All the result will be displayed below the search row.

![search result](https://i.imgur.com/lzB0xYL.jpg)

- Color 
    - `SteelBlue` `#4682B4` Watching
    - `DarkGoldenRod` `#B8860B` Planned
    - `seagreen` `#2E8B57` Completed
    - `RebeccaPurple` `#663399` Rewatched
    - `IndianRed` `#cd5c5c` Paused
    - `DarkRed` `#8B0000` Dropped

By clicking on a cover, you can see some more useful details about the anime.

![anime details](https://i.imgur.com/bUArTo9.png)

<h3>Random</h3>

<h4>Random filters</h4>

The random row contains some filters.

![random filters](https://i.imgur.com/XLvlz7V.png)

- The ___username___ part is mandatory, as the project will get your own anime list.
- The ___maximum number of episodes___ default value is 24 but it can go from 1 to 2000.
- You can select some ___genre___ or ___format___. If you search for a specific format/genre, you can enter it in the design are.

<h4>Random result</h4>

The algorithm will fin an anime in your _planned_ that match your filter.

![random result](https://i.imgur.com/MNCmpcm.png)

You will see some information such as:
- The name
- The description
- The number of episode
- The genre
- The origin
- etc.
