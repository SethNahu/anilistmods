const delay = ms => new Promise(res => setTimeout(res, ms));

const preview_picture_challenge = new Map([
    [62649, "https://i.postimg.cc/d0QxQQzB/1.png"], [61647, "https://i.imgur.com/mVnXYNx.png"],
    [61848, "https://i.imgur.com/phYOThq.png"], [4448, "https://i.imgur.com/hHLL1la.png"],
    [5027, "https://i.imgur.com/cDwKIk7.png"], [8462, "https://i.imgur.com/DmyKQjY.png"],
    [38849, "https://i.imgur.com/DRQPkyF.png"], [6171, "https://imgur.com/HlDTUwT.png"],
    [28272, "https://i.imgur.com/enKkOBq.png"], [6553, "https://i.imgur.com/yrzEyoH.png"],
    [40936, "https://i.imgur.com/RtNGUzH.png"], [7738, "https://i.imgur.com/DlTemfo.png"],
    [9282, "https://i.imgur.com/2McQjTk.png"], [9777, "https://i.imgur.com/zsHovXE.png"],
    [14236, "https://i.imgur.com/8KbnB5X.png"], [22800, "https://i.imgur.com/CccSt2j.png"],
    [27098, "https://i.imgur.com/CjarH7h.png"], [39077, "https://i.imgur.com/n1XvQRN.png"],
    [45664, "https://imgur.com/6cMsFO8.png"], [59986, "https://i.imgur.com/Jd51NXE.png"],
    [7284, "https://i.imgur.com/pTROGBZ.png"], [9352, "https://i.imgur.com/QIwooqi.png"],
    [13775, "https://i.imgur.com/qA0w8Zk.png"], [36321, "https://i.imgur.com/EZ3RYJ2.png"],
    [7566, "https://i.imgur.com/dGEZoT5.png"], [8037, "https://i.imgur.com/nHafE4u.png"],
    [12963, "https://i.imgur.com/89fcVgh.png"], [17846, "https://i.imgur.com/CvqpITd.png"],
    [25322, "https://i.imgur.com/5coj1PE.png"], [25914, "https://i.imgur.com/eIKEAIs.png"],
    [40441, "https://i.imgur.com/bXL9ygM.png"], [44858, "https://i.imgur.com/jYNiEoV.png"],
    [47806, "https://i.imgur.com/qmZkqrr.png"], [53566, "https://i.imgur.com/461C8LE.png"],
    [58215, "https://i.postimg.cc/SsGyk7ny/b2XP9xzs.png"], [59212, "https://i.imgur.com/t8xSUP9.png"],
    [7567, "https://i.imgur.com/zzgQgUk.png"], [9002, "https://i.imgur.com/DoaN3ON.png"],
    [16264, "https://i.imgur.com/9GXr9At.png"], [19773, "https://i.imgur.com/K4y2Td2.png"],
    [58423, "https://imgur.com/et4Q05F.png"], [50869, "https://i.imgur.com/N3W5l0u.png"],
    [28916, "https://i.imgur.com/L4dLJxL.png"], [61944, "https://i.imgur.com/WYWjFH3.png"],
    [49205, "https://i.imgur.com/mc8kO1U.png"], [22395, "https://i.imgur.com/Ke1F3va.png"],
    [10377, "https://i.imgur.com/9izW3fU.png"], [7104, "https://i.imgur.com/Z7S0uq4.png"],
    [13347, "https://i.imgur.com/AQ6RL6E.png"], [39961, "https://i.imgur.com/jfcrtDQ.png"],
    [57229, "https://i.imgur.com/EdutAmX.png"], [59780, "https://i.imgur.com/0cej7k3.png"],
    [51196, "https://i.imgur.com/QSi2tzE.png"], [11405, "https://i.imgur.com/kYnpBTL.png"],
    [13560, "https://i.imgur.com/6FvPhMJ.png"],  [17048, "https://i.imgur.com/ZbNPCbm.png"],
    [30744, "https://i.imgur.com/osQm9Oa.png"], [46745, "https://i.imgur.com/04RTJVh.png"],
    [52011, "https://i.imgur.com/ZhMQ7sT.png"], [56903, "https://i.imgur.com/Zy0Uyuy.png"],
    [60975, "https://imgur.com/mcLljYo.png"], [5288, "https://i.imgur.com/QMHqkNp.png"],
    [6111, "https://i.imgur.com/0PL3hBj.png"], [5289, "https://i.imgur.com/iG1JE8i.png"],
    [5875, "https://i.imgur.com/tPVdKPr.png"], [6631, "https://i.imgur.com/G3IjubP.png"],
    [5290, "https://i.imgur.com/nJzNNAr.png"], [5559, "https://i.imgur.com/oBKYpQ3.png"],
    [5558, "https://i.imgur.com/2bu8b50.png"], [6376, "https://imgur.com/WCNLiLf.png"],
    [6112, "https://i.imgur.com/Kx3e97D.png"], [6375, "https://i.imgur.com/xAZLuof.png"],
    [5557, "https://i.imgur.com/Pp0IRKu.png"], [5291, "https://i.imgur.com/0ZvErLt.png"],
    [5292, "https://i.imgur.com/WxbhFbM.png"], [5293, "https://i.imgur.com/UtQFAnn.png"],
    [5876, "https://i.imgur.com/dfczj45.png"], [6630, "https://i.imgur.com/Qeo8CHX.png"],
    [5556, "https://i.imgur.com/u09xKxO.png"], [6395, "https://imgur.com/tAwnxK8.png"],
    [10665, "https://i.imgur.com/BsteVCB.png"], [51476, "https://i.imgur.com/2fUEA5i.png"],
    [7196, "https://i.imgur.com/xY0NVWt.png"], [7915, "https://i.imgur.com/akK8LP4.png"],
    [8889, "https://i.imgur.com/FeGOaoQ.png"], [9675, "https://i.imgur.com/bnR2U6E.png"],
    [11261, "https://i.imgur.com/Im03NzF.png"], [12658, "https://i.imgur.com/Fpl7DiB.png"],
    [17325, "https://i.imgur.com/HyIVexT.png"], [19440, "https://i.imgur.com/zi5b3lk.png"],
    [26466, "https://i.imgur.com/I6JQbdM.png"], [32949, "https://i.imgur.com/buUAInK.png"],
    [42357, "https://i.imgur.com/aDE1mRY.png"], [47135, "https://i.imgur.com/aZKhF0Y.png"],
    [49909, "https://i.imgur.com/728IOrL.png"], [52601, "https://i.imgur.com/7Hl5MDS.png"],
    [55870, "https://imgur.com/GaEjAk4.png"], [60670, "https://i.imgur.com/smbzJkJ.png"],
    [9960, "https://imgur.com/uDiN5Xp.png"], [16045, "https://i.imgur.com/uSqiqZU.png"],
    [23111, "https://i.imgur.com/WYjN16H.png"], [38004, "https://i.imgur.com/e0NecOT.png"],
    [46422, "https://i.imgur.com/GxgNKlN.png"], [61414, "https://i.imgur.com/XaZWme9.png"]]);

const timed_limit = [62649, 61647, 61848];
const tier_challenge = [4448, 5027, 8462, 38849];
const special = [6171, 28272, 6553, 40936, 7738, 9282,  9777, 14236,  22800, 27098,  39077, 45664,  59986];
const studio = [7284, 9352, 13775, 36321];
const franchise = [7566, 8037, 12963, 17846, 25322, 25914, 40441, 44858, 47806, 53566, 58215, 59212];
const legend = [7567, 9002, 16264, 19773, 58423];
const anthologies = [50869, 28916];
const classic = [61944, 49205,  22395, 10377, 7104, 13347, 39961, 57229, 59780, 51196];
const puzzle = [11405, 13560, 17048, 30744, 46745, 52011, 56903, 60975];
const genres = [5288, 6111, 5289, 5875, 6631, 5290, 5559, 5558, 6376, 6112, 6375, 5557, 5291, 5292, 5293, 5876, 6630, 5556];

let all_challenge_id = []
all_challenge_id = all_challenge_id.concat(timed_limit).concat(tier_challenge).concat(special).concat(studio).concat(franchise).concat(legend).concat(anthologies).concat(classic).concat(puzzle).concat(genres);

const tier_challenge_manga = [6395, 10665, 51476];
const manga_city = [7196, 7915, 8889, 9675, 11261, 12658, 17325, 19440, 26466, 32949, 42357, 47135, 49909, 52601, 55870, 60670];
const special_manga = [9960, 16045, 23111, 38004, 46422, 61414];

let all_challenge_manga_id = []
all_challenge_manga_id = all_challenge_manga_id.concat(tier_challenge_manga).concat(manga_city).concat(special_manga);

const associate = new Map([["timed-limit", "Timed Limit"], ["tier", "Tier"], ["special", "Special"],
    ["studio", "Studio"], ["franchise", "Franchise"], ["legend", "Legend"], ["anthologies", "Anthologies"],
    ["classic", "Classic"], ["puzzle", "Puzzle"], ["genre", "Genre"], ["manga-city", "Manga City"]]);

function which_category(id)
{
    if (timed_limit.includes(id))
        return "timed-limit";
    else if (tier_challenge.includes(id) || tier_challenge_manga.includes(id))
        return "tier";
    else if (special.includes(id) || special_manga.includes(id))
        return "special";
    else if (studio.includes(id))
        return "studio";
    else if (franchise.includes(id))
        return "franchise";
    else if (legend.includes(id))
        return "legend";
    else if (anthologies.includes(id))
        return "anthologies";
    else if (classic.includes(id))
        return "classic";
    else if (puzzle.includes(id))
        return "puzzle";
    else if (manga_city.includes(id))
        return "manga-city"
    return "genre";
}

let all_elements = new Map([]);
let all_challenge = new Map([]);

async function display_to_it(url, id, exist)
{
    let category = which_category(id);
    let elementExists = null;
    let allChallenge = null;
    if (all_elements.has(category) === false) {
        elementExists = document.createElement("div");
        elementExists.className = "col challenge-list";
        elementExists.id = category;
        let title = document.createElement("h2");
        title.className = "challenge-category";
        title.innerHTML = associate.get(category);
        elementExists.appendChild(title);
        let separation = document.createElement("hr");
        separation.className = "separation";
        elementExists.appendChild(separation);
        allChallenge = document.createElement("div");
        allChallenge.className = "horizontal-scroll-wrapper";
        allChallenge.id = category + "-challenge";
        elementExists.appendChild(allChallenge);
    } else {
        elementExists = all_elements.get(category);
        allChallenge = all_challenge.get(category + "-challenge");
    }
    let challenge = document.createElement("div");
    let challenge_url = document.createElement("a");
    let challenge_picture = document.createElement("img");
    if (exist === true) {
        challenge_picture.className = "challenge-picture";
        challenge.className = "challenge-preview";
    } else {
        challenge_picture.className = "challenge-picture-not";
        challenge.className = "challenge-preview not";
    }
    challenge_picture.src = preview_picture_challenge.get(id);
    challenge_url.href = url;
    challenge_url.appendChild(challenge_picture);
    challenge.appendChild(challenge_url);
    allChallenge.appendChild(challenge);
    all_elements.set(category, elementExists);
    all_challenge.set(category + "-challenge", allChallenge);
}

let possessed_challenge = [];

async function get_all_user_comment_thread(userId, page) {
    let query = "query ($page: Int, $userId: Int) {\n" +
        "  Page(page: $page) {\n" +
        "    pageInfo {\n" +
        "      hasNextPage\n" +
        "    }\n" +
        "    threadComments(userId: $userId) {\n" +
        "      siteUrl\n" +
        "      id\n" +
        "      threadId\n" +
        "    }\n" +
        "  }\n" +
        "}\n";
    let my_variables = {page, userId};
    var url = 'https://graphql.anilist.co',
        options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                query: query,
                variables: my_variables
            })
        };
    await fetch(url, options).then(response => {
        if (response.ok) {
            return response.json();
        }
        throw new Error('Something went wrong');
        })
        .then(async data => {
            const checker = document.getElementById("checked-slider");
            for (let i = 0; i < data.data.Page.threadComments.length; i++) {
                if (!checker.checked && all_challenge_id.includes(data.data.Page.threadComments[i].threadId) === true && possessed_challenge.includes(data.data.Page.threadComments[i].threadId) === false) {
                    await display_to_it(data.data.Page.threadComments[i].siteUrl, data.data.Page.threadComments[i].threadId, true)
                    possessed_challenge.push(data.data.Page.threadComments[i].threadId);
                } else if (checker.checked && all_challenge_manga_id.includes(data.data.Page.threadComments[i].threadId) === true && possessed_challenge.includes(data.data.Page.threadComments[i].threadId) === false) {
                    await display_to_it(data.data.Page.threadComments[i].siteUrl, data.data.Page.threadComments[i].threadId, true)
                    possessed_challenge.push(data.data.Page.threadComments[i].threadId);
                }
            }
            if (data.data.Page.pageInfo.hasNextPage === true) {
                await get_all_user_comment_thread(userId, page + 1);
            }
        });
}

function sort_category(category)
{
    if (all_elements.has(category) && all_challenge.has(category + "-challenge")) {
        let allChallenge = all_elements.get(category);
        let allPreview = all_challenge.get(category + "-challenge").children;
        let previewGroup = all_challenge.get(category + "-challenge");
        let row = document.getElementById("preview-challenge-all");

        let not = []
        let presentIn = []
        for (let i = 0; i < allPreview.length; i++) {
            if (allPreview[i].className.includes("not")) {
                not.push(allPreview[i]);
            } else {
                presentIn.push(allPreview[i]);
            }
        }
        for (let i = 0; i < allPreview.length; i++)
            previewGroup.removeChild(allPreview[i]);
        for (let i = 0; i < presentIn.length; i++)
            previewGroup.appendChild(presentIn[i]);
        for (let i = 0; i < not.length; i++)
            previewGroup.appendChild(not[i]);
        row.appendChild(allChallenge);
    }
}


async function get_user_information() {
    let nameUser = document.getElementById("user-name").value;
    let row = document.getElementById("preview-challenge-all");
    let rowChild = document.getElementById("preview-challenge-all").children;

    let awaitDoc = document.createElement("img");
    awaitDoc.src = "img/loading.gif";
    awaitDoc.className = "await-center";
    row.appendChild(awaitDoc);

    if (nameUser.trim().length === 0)
        return;
    let query = "query ($nameUser: String) {\n" +
        "  User(name: $nameUser) {\n" +
        "    id\n" +
        "  }\n" +
        "}\n";
    let my_variables = {nameUser};
    var url = 'https://graphql.anilist.co',
        options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                query: query,
                variables: my_variables
            })
        };
    for (let i = 0; i < rowChild.length; i++)
        row.removeChild(rowChild[i]);
    await fetch(url, options).then(response => response.json())
        .then(async data => {
            await get_all_user_comment_thread(data.data.User.id, 1);
        });
    const checker = document.getElementById("checked-slider");
    let ids = [];
    if (checker.checked)
        ids = all_challenge_manga_id;
    else
        ids = all_challenge_id;
    for (let i = 0; i < ids.length; i++) {
        if (possessed_challenge.includes(ids[i]) === false) {
            await display_to_it("https://anilist.co/forum/thread/" + ids[i].toString() , ids[i], false);
        }
    }
    associate.forEach((value, key) => {
        sort_category(key);
    });
    possessed_challenge = []
}

/*
async function parse_all_challenge(data)
{
    let lines = data.split("\n");
    let challenges = []
    for (let i = 0; i < lines.length; i++) {
        if (lines[i].includes("https://anilist.co")) {
            let obj = {}
            let info = lines[i].split("[").join(',').split(']').join(',').split('(').join(',').split(')').join(',').split(',');
            info = info.filter(item => item);
            info = info.filter((v) => v !== ' ')
            obj["name"] = info[0]
            obj["link"] = info[1]
            let url = info[1].split('/')
            url = url.filter(item => item);
            url = url.filter((v) => v !== ' ')
            obj["threadId"] = parseInt(url[url.length - 1]);
            challenges.push(obj);
        }
    }
    challenges.shift();
    await get_user_information();
}

async function get_all_challenge() {
    let activityId = 26266744
    let query = "query ($activityId: Int) {\n" +
        "  Activity(id: $activityId) {\n" +
        "    __typename ... on TextActivity {\n" +
        "      text\n" +
        "    }\n" +
        "  }\n" +
        "}";
    let my_variables = {activityId};
    var url = 'https://graphql.anilist.co',
        options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                query: query,
                variables: my_variables
            })
        };
    fetch(url, options).then(response => response.json())
        .then(async data => {
            await parse_all_challenge(data.data.Activity.text);
        });
}
*/
document.getElementById("button-search-challenge").addEventListener("click", get_user_information);
get_user_information();
