const picture_already_in = new Map([
    ["https://anilist.co/anime/416", "https://drive.google.com/uc?export=view&id=1k6Ak522pz0UbpoLjLP_HdgnLmialMQmW"],
    ["https://anilist.co/anime/585", "https://drive.google.com/uc?export=view&id=1JmKQtUV7jRxFF2CFZVYhNyRpworZfxYV"],
    ["https://anilist.co/anime/2107", "https://drive.google.com/uc?export=view&id=1RDzIpWLLMfJQga2kWGZHttc597ZXaaYq"],
    ["https://anilist.co/anime/2890", "https://drive.google.com/uc?export=view&id=1Rcepfj1ivjoAWtGn2scDypkOwYrqlyML"],
    ["https://anilist.co/anime/12859", "https://drive.google.com/uc?export=view&id=1-rDt6xhAqb0KWYq1kzUvhAlBxr7R9LkO"],
    ["https://anilist.co/anime/18689", "https://drive.google.com/uc?export=view&id=1tZnwBnLyHLJ79GdytXDWAWWSju55yL4o"],
    ["https://anilist.co/anime/20755", "https://drive.google.com/uc?export=view&id=1pnd0v2mxzT3VQNPtt1iozlZ2pi8OYium"],
    ["https://anilist.co/anime/21131", "https://drive.google.com/uc?export=view&id=1Zki8tngQQ32gfO75mvebebnREiPN2p1a"],
    ["https://anilist.co/anime/20727", "https://drive.google.com/uc?export=view&id=1oE2L_6HhkhV0HPG5rBrkSxShQmzlmNjO"],
    ["https://anilist.co/anime/20931", "https://drive.google.com/uc?export=view&id=15AunM4gVaHD3kfNyFFeQWy1SFLn193GC"],
    ["https://anilist.co/anime/20792", "https://drive.google.com/uc?export=view&id=1BrG1B9GAlYVdnJbvaD1LC8pd1vqd12IT"],
    ["https://anilist.co/anime/20923", "https://drive.google.com/uc?export=view&id=10LZVfrxn0KbApppLlSg_vsxeWcd7Z2qQ"],
    ["https://anilist.co/anime/20994", "https://drive.google.com/uc?export=view&id=1Xk8YmT-pUkYO6lRsvJGjnJ1y1uoZ7hf5"],
    ["https://anilist.co/anime/20992", "https://drive.google.com/uc?export=view&id=1lqaoOsZpv2YcoP8rrHM-xO79qXcSFufV"],
    ["https://anilist.co/anime/20920", "https://drive.google.com/uc?export=view&id=1lArk090TVEnQqBvQrZf8tFYH-_uLmUJm"],
    ["https://anilist.co/anime/20725", "https://drive.google.com/uc?export=view&id=1PHfygD7f53kWmm2szfcHI6BsPE60nMTJ"],
    ["https://anilist.co/anime/21087", "https://drive.google.com/uc?export=view&id=1-LVEPea_BYWZDl3QX5MF3TksDP92LOea"],
    ["https://anilist.co/anime/20807", "https://drive.google.com/uc?export=view&id=1KNrZtTvsjn-FYOHmw848l6EzAvG4xS0E"],
    ["https://anilist.co/anime/20829", "https://drive.google.com/uc?export=view&id=1-_GUBtmq6pp5xSkB-xu1HrowfD8meQUQ"],
    ["https://anilist.co/anime/20993", "https://drive.google.com/uc?export=view&id=1Zo40V1hqZFt2VEecDSwVqWPDgAW5SI8w"],
    ["https://anilist.co/anime/20850", "https://drive.google.com/uc?export=view&id=1Xv-TBZWWRtlAO4NxpWO7OfPI4dLHf3oB"],
    ["https://anilist.co/anime/20954", "https://drive.google.com/uc?export=view&id=1-HPU6pq0CPL0Ib0AH1gmOeEvFnC0YBKx"],
    ["https://anilist.co/anime/21170", "https://drive.google.com/uc?export=view&id=1uwIFtBfjtMa1W_0aCW3PWw-KqYMR0BYz"],
    ["https://anilist.co/anime/21311", "https://drive.google.com/uc?export=view&id=1eeHmjVnoFhusnKD3c-r4FehFSMhulgi0"],
    ["https://anilist.co/anime/21518", "https://drive.google.com/uc?export=view&id=1DLGTs3jYISnjBiK25RgSRxhNV-uBFkrG"],
    ["https://anilist.co/anime/21364", "https://drive.google.com/uc?export=view&id=1Eh01jM24VAwrK38zdkjIRxRPzLu7I2sZ"],
    ["https://anilist.co/anime/21698", "https://drive.google.com/uc?export=view&id=1ckGhbsZlij4HKR_rw8JTZx2-dBMUm8fn"],
    ["https://anilist.co/anime/21394", "https://drive.google.com/uc?export=view&id=1CnbHlDC3QWIZ6mIqyJbw1hXDEMBLxoQv"],
    ["https://anilist.co/anime/21366", "https://drive.google.com/uc?export=view&id=1kszGwAw1Pw1qb93FfyxKq2lSWZ0IyVOs"],
    ["https://anilist.co/anime/21459", "https://drive.google.com/uc?export=view&id=1wYEeqkj-sxLjM7GgLrVSYa89MPkHZV5u"],
    ["https://anilist.co/anime/21221", "https://drive.google.com/uc?export=view&id=19PgbV470Ri_2-wbG0NyLoJYY0EbOiSs7"],
    ["https://anilist.co/anime/21390", "https://drive.google.com/uc?export=view&id=16MqfFWZ2h4IZ_OD7_JcbBqyFL3LstIOn"],
    ["https://anilist.co/anime/21519", "https://drive.google.com/uc?export=view&id=1Gkd4JLhjbvF1lVwd1A6-f3twtv8DHLVF"],
    ["https://anilist.co/anime/21709", "https://drive.google.com/uc?export=view&id=1gnUMvj3fC3TXX-Q7SD03rg1yd6mC4R9o"],
    ["https://anilist.co/anime/20958", "https://drive.google.com/uc?export=view&id=1AkKD7EtZagUbUPzpPJLgI1ABCk8kDq-S/"],
    ["https://anilist.co/anime/97940", "https://drive.google.com/uc?export=view&id=1W7nufWjklDpsySd0TftkfYorXw6VbjFi"],
    ["https://anilist.co/anime/97886", "https://drive.google.com/uc?export=view&id=10wVuv9X6ReJJeNnFEtNPBXs86s6PUrT_"],
    ["https://anilist.co/anime/98449", "https://drive.google.com/uc?export=view&id=1Itkg5UP_dcSqWIEPQR3eqwXZc-aQ4tZ6"],
    ["https://anilist.co/anime/98659", "https://drive.google.com/uc?export=view&id=1j7SMFqgQxgjIv5Co48zXgHRMpYi-Qbdy"],
    ["https://anilist.co/anime/99255", "https://drive.google.com/uc?export=view&id=16PEUNg_wEG7Ll1q5mAzIcJdwO2uuZF_d"],
    ["https://anilist.co/anime/98314", "https://drive.google.com/uc?export=view&id=1FDfhDrjiRysJbpIMqyG6qlRVV8v3iQ_r"],
    ["https://anilist.co/anime/21858", "https://drive.google.com/uc?export=view&id=1LQOmfnNK_u4VFGuCwrdY3afDneWfH9ex"],
    ["https://anilist.co/anime/97986", "https://drive.google.com/uc?export=view&id=19Oz0iiYrtzNXo0Uw9onMaPhO1PbVL3GG"],
    ["https://anilist.co/anime/87486", "https://drive.google.com/uc?export=view&id=1PkcrfG71Yp8ChDnq2nh-SmK77nUNLxWr"],
    ["https://anilist.co/anime/21856", "https://drive.google.com/uc?export=view&id=1LQc1LAj82ii228ZQj45TQH9-NVuAIWsp"],
    ["https://anilist.co/anime/21676", "https://drive.google.com/uc?export=view&id=1YW0SFez4clbijiLLcdYmfOrGaC8Izt_-"],
    ["https://anilist.co/anime/97645", "https://drive.google.com/uc?export=view&id=1sYEcNJrddUWNpYZ799ulhEpLckDFK9zw"],
    ["https://anilist.co/anime/21377", "https://drive.google.com/uc?export=view&id=1wHtu9SdP129JE3rciq6RJqKlfCRubipd"],
    ["https://anilist.co/anime/21827", "https://drive.google.com/uc?export=view&id=1VRimcqOur7ywudwm3wF_3zm_U4wp1Npt"],
    ["https://anilist.co/anime/99423", "https://drive.google.com/uc?export=view&id=1dvyZPsaiYoh3F1LRtj5k9xd9lU5pALjw"],
    ["https://anilist.co/anime/100723", "https://drive.google.com/uc?export=view&id=19SO25EO1mzzy4L2KrgMwhMGUuaRVy5v9"],
    ["https://anilist.co/anime/105143", "https://bit.ly/3q4M22a"],
    ["https://anilist.co/anime/109190", "https://drive.google.com/uc?export=view&id=1-VBeauoCavg1axrDp8ghsJaWfmJBnZ_i"],
    ["https://anilist.co/anime/114124", "https://bit.ly/3TCxiW1"],
    ["https://anilist.co/anime/115230", "https://drive.google.com/uc?export=view&id=1vlyjZoKT__9gZa_a4GtpeQt1q_fXqLch"],
    ["https://anilist.co/anime/116589", "https://drive.google.com/uc?export=view&id=1ea2yAVMLokp9qjL_RLYfFW52XaU2diMn"],
    ["https://anilist.co/anime/126356", "https://drive.google.com/uc?export=view&id=12qSdUy96RFiZ53WNHosc1SNkzAanYvaW"],
    ["https://anilist.co/anime/127720", "https://drive.google.com/uc?export=view&id=1Hp5nfJHPw9FKLx1ZSEfQl1AIsmyfUnDA"],
    ["https://anilist.co/anime/108465", "https://drive.google.com/uc?export=view&id=10YWEaqDult6rKtzt0vYLVYI01jrqrwnk"],
    ["https://anilist.co/anime/108511", "https://bit.ly/3TFCbNS"],
    ["https://anilist.co/anime/116742", "https://drive.google.com/uc?export=view&id=1T9Imifmah_6R_z7ufnUHoRLZISEJ9rqe"],
    ["https://anilist.co/anime/131646", "https://drive.google.com/uc?export=view&id=1iLVwaQv4sxJD8XXC2f6DrsD-mNABz0gw"],
    ["https://anilist.co/anime/131681", "https://drive.google.com/uc?export=view&id=1eVJGp7F0nDBMMbjTUuj0iwDfSdX8kjig"],
    ["https://anilist.co/anime/142455", "https://drive.google.com/uc?export=view&id=1MJN6lg-cZ8NcLK5d-eLs0aBjwxt6aRNL"],
    ["https://anilist.co/anime/132171", "https://drive.google.com/uc?export=view&id=1X46Ew4LZZiryitwVDIYkk-Yr9FfWUzaf"],
    ["https://anilist.co/anime/142329", "https://drive.google.com/uc?export=view&id=1EMkWwJLag1uDKuZnZMycJlnAvaKXVfSD"],
    ["https://anilist.co/anime/111321", "https://drive.google.com/uc?export=view&id=1SlurKSR6kCAgb2msEgF_pa7gpws-nl6Y"],


    ["https://anilist.co/anime/137822", "https://i.imgur.com/TO9re9s.png"],
    ["https://anilist.co/anime/139630", "https://i.imgur.com/XgSDkZ5.jpg"],
    ["https://anilist.co/anime/151806", "https://i.imgur.com/BCUjFDk.jpg"],
    ["https://anilist.co/anime/155907", "https://i.imgur.com/1UEyDrF.jpg"],
    ["https://anilist.co/anime/137909", "https://i.imgur.com/yaAaqoQ.png"],
    ["https://anilist.co/anime/144553", "https://i.imgur.com/8SDMOy9.jpg"],
    ["https://anilist.co/anime/148116", "https://i.imgur.com/sRVVbC8.jpg"],





    ["https://anilist.co/anime/141014", "https://drive.google.com/uc?export=view&id=16PENnjdWMEBbVjuEK4HX8SEc0fDXxGjc"]

]);

let cpy =["", ""];

function recoverSelectedElement(listName, replaceStr) {
    let list = [];
    let listChildren = document.getElementById(listName).children;
    for (let i = 0; i < listChildren.length; i++) {
        if (listChildren.item(i).className === "task selected-genre")
            list.push(listChildren.item(i).id.replace(replaceStr, ''));
    }
    return list;
}

function addListToUrl(list, name) {
    let str = "";
    for (let i = 0; i < list.length; i++) {
        str += "&" + name + "=" + list.at(i);
    }
    if (str.trim().length === 0)
        return "&" + name + "=";
    return str;
}

/* Submit search */

function recoverSelectedElementTag(listName, replaceStr) {
    let list = [];
    let listChildren = document.getElementById(listName).children;
    for (let i = 0; i < listChildren.length; i++) {
        if (listChildren.item(i).className === "task-grp selected-genre")
            list.push(listChildren.item(i).id.replace(replaceStr, ''));
    }
    return list;
}
const element_change = new Map([["Completed", "COMPLETED"], ["Watching", "CURRENT"],
    ["Rewatching", "REPEATING"], ["Planned", "PLANNING"], ["Dropped", "DROPPED"], ["Paused", "PAUSED"]]);

function recoverSelectedElementChange(listName, replaceStr) {
    let list = [];
    let listChildren = document.getElementById(listName).children;
    for (let i = 0; i < listChildren.length; i++) {
        if (listChildren.item(i).className === "task selected-genre")
            list.push(element_change.get(listChildren.item(i).id.replace(replaceStr, '')));
    }
    return list;
}

function remove_unwanted_state(data, state_list) {
    let tmp_list = []
    for (let i = 0; i < data.length; i++) {
        if (state_list.includes(data.at(i).status)) {
            tmp_list.push(data.at(i));
        }
    }
    return tmp_list;
}

function remove_unwanted_genre(anime_list, list_genre)
{
    let tmp_list = []
    for (let i = 0; i < anime_list.length; i++) {
        let isInside = true;
        for (let j = 0; j < list_genre.length; j++) {
            if (anime_list.at(i).media.genres.includes(list_genre.at(j)) === false) {
                isInside = false;
                break;
            }
        }
        if (isInside)
            tmp_list.push(anime_list.at(i));
    }
    return tmp_list;
}

function remove_unwanted_format(anime_list, format)
{
    let tmp_list = []

    for (let i = 0; i < anime_list.length; i++) {
        let isInside = true;
        for (let j = 0; j < format.length; j++) {
            if (format.at(j).toUpperCase() !== anime_list.at(i).media.format) {
                isInside = false;
                break;
            }
        }
        if (isInside)
            tmp_list.push(anime_list.at(i));
    }
    return tmp_list;
}

function remove_unwanted_year(anime_list, year)
{
    let tmp_list = []
    for (let i = 0; i < anime_list.length; i++) {
        if (anime_list[i].media.seasonYear !== null && year.includes(anime_list[i].media.seasonYear.toString())) {
            tmp_list.push(anime_list.at(i));
        }
    }
    return tmp_list;
}

function remove_unwanted_season(anime_list, season)
{
    let tmp_list = []
    for (let i = 0; i < anime_list.length; i++) {
        if (anime_list[i].media.season !== null && season.includes(anime_list[i].media.season)) {
            tmp_list.push(anime_list.at(i));
        }
    }
    return tmp_list;
}

function remove_unwanted_tag(anime_list, tagsList)
{
    let tmp_list = []
    for (let i = 0; i < anime_list.length; i++) {
        for (let j = 0; j < tagsList.length; j++) {
            const res = anime_list.at(i).media.tags.filter(obj => Object.values(obj).some(val => val.includes(tagsList.at(j))));
            if (res.length > 0) {
                tmp_list.push(anime_list.at(i));
                break;
            }
        }
    }
    return tmp_list;
}

function clean_array(func, list, filter)
{
    let new_list = []
    if (filter.length > 0) {
        for (let i = 0; i < list.length; i++) {
            let tmp = func(list.at(i).entries, filter);
            if (tmp.length > 0) {
                list.at(i).entries = tmp;
                new_list.push(list.at(i));
            }
        }
    } else
        new_list = list;
    return new_list;
}

function parse_data(data)
{
    let state_list = recoverSelectedElementChange('listState', 'state-selector-check-');
    let genre_list = recoverSelectedElement('list', 'genre-selector-check-');
    let formatList = recoverSelectedElement('listFormat', 'format-selector-check-');
    let tagList = recoverSelectedElementTag('tag-list', 'tag-selector-check-');
    let yearList = recoverSelectedElement('listYear', 'year-selector-check-');
    let seasonList = recoverSelectedElement('listSeason', 'season-selector-check-');
    
    let list_data = clean_array(remove_unwanted_state, data.data.MediaListCollection.lists, state_list);
    let new_list_data = clean_array(remove_unwanted_genre, list_data, genre_list);
    let new_list_data_format = clean_array(remove_unwanted_format, new_list_data, formatList);
    let new_list_data_year = clean_array(remove_unwanted_year, new_list_data_format, yearList);
    let new_list_data_season = clean_array(remove_unwanted_season, new_list_data_year, seasonList);
    return clean_array(remove_unwanted_tag, new_list_data_season, tagList);
}

function get_list_already_in(data, datas)
{
    let list_anime = [];
    for (let i = 0; i < data.ThreadComment.length; i++) {
        let tmp = data.ThreadComment[i].comment.split('\n');
        for (let j = 0; j < tmp.length; j++) {
            if (tmp[j].includes("https://anilist.co"))
                list_anime.push(tmp[j]);
        }
    }
    let new_list_data = []
    if (list_anime.length > 0) {
        for (let i = 0; i < datas.length; i++) {
            let tmp = []
            for (let j = 0; j < datas[i].entries.length; j++) {
                if (list_anime.indexOf(datas[i].entries[j].media.siteUrl) === -1) {
                    tmp.push(datas[i].entries[j]);
                }
            }
            if (tmp.length > 0) {
                datas.at(i).entries = tmp;
                new_list_data.push(datas[i]);
            }
        }
    } else {
        return datas;
    }
    return new_list_data;
}

function get_challenge_principal(threadId, datas) {
    let query = "query ($threadId: Int) {\n" +
        "  ThreadComment(id: $threadId) {\n" +
        "    id\n" +
        "    siteUrl\n" +
        "    createdAt\n" +
        "    comment\n" +
        "  }\n" +
        "}";
    let my_variables = {threadId};
    var url = 'https://graphql.anilist.co',
        options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                query: query,
                variables: my_variables
            })
        };
    fetch(url, options).then(response => response.json())
        .then(data => {
            let res = get_list_already_in(data.data, datas);
            display_data(res);
        });
}

function create_element_to_display(data) {
    let all_data = parse_data(data);
    let challenge = document.getElementById('challenge').value;
    if (challenge != null && challenge !== "") {
        let tmp = challenge.split('/');
        get_challenge_principal(parseInt(tmp[tmp.length - 1]), all_data)
    } else {
        display_data(all_data);
    }
}

function fetch_search(userName, startAtStr, endAtStr) {
    let startQuery = "query ($userName: String, $type: MediaType";
    let mediaStart = "  MediaListCollection(userName: $userName, type: $type";
    let type = "ANIME"
    let my_variables = {userName, type};
   // get_challenge_principal()

    if (startAtStr != null && endAtStr === "" && startAtStr !== "") {
        startQuery += ", $startAt: FuzzyDateInt";
        mediaStart += ", startedAt_greater: $startAt";
        let startAt = parseInt(startAtStr);
        my_variables = {userName, type, startAt};
    }
    else if (endAtStr != null && startAtStr === "" && endAtStr !== "") {
        startQuery += ", $endAt: FuzzyDateInt";
        mediaStart += ", completedAt_greater: $endAt";
        let endAt = parseInt(endAtStr);
        my_variables = {userName, type, endAt};
    } else if (endAtStr != null && endAtStr !== "" && startAtStr != null && startAtStr !== "") {
        startQuery += ", $startAt: FuzzyDateInt";
        mediaStart += ", startedAt_greater: $startAt";
        startQuery += ", $endAt: FuzzyDateInt";
        mediaStart += ", completedAt_greater: $endAt";
        let startAt = parseInt(startAtStr);
        let endAt = parseInt(endAtStr);
        my_variables = {userName, type, startAt, endAt};
    }
    let query = startQuery + ") {\n" +
        mediaStart + ") {\n" +
        "    lists {\n" +
        "      name\n" +
        "      status\n" +
        "      isCustomList\n" +
        "      isCompletedList: isSplitCompletedList\n" +
        "      entries {\n" +
        "        ...mediaListEntry\n" +
        "      }\n" +
        "    }\n" +
        "  }\n" +
        "}\n" + "\n" +
        "fragment mediaListEntry on MediaList {\n" +
        "  status\n" +
        "  startedAt {\n" +
        "    year\n" +
        "    month\n" +
        "    day\n" +
        "  }\n" +
        "  completedAt {\n" +
        "    year\n" +
        "    month\n" +
        "    day\n" +
        "  }\n" +
        "  media {\n" +
        "    id\n" +
        "    siteUrl\n" +
        "    tags {\n" +
        "        name\n" +
        "    }\n" +
        "    season\n" +
        "    seasonYear\n" +
        "    title {\n" +
        "      userPreferred\n" +
        "      romaji\n" +
        "      english\n" +
        "      native\n" +
        "    }\n" +
        "    coverImage {\n" +
        "      extraLarge\n" +
        "      large\n" +
        "    }\n" +
        "    type\n" +
        "    format\n" +
        "    status(version: 2)\n" +
        "    description\n" +
        "    episodes\n" +
        "    duration\n" +
        "    averageScore\n" +
        "    popularity\n" +
        "    countryOfOrigin\n" +
        "    genres\n" +
        "    bannerImage\n" +
        "    startDate {\n" +
        "      year\n" +
        "      month\n" +
        "      day\n" +
        "    }\n" +
        "  }\n" +
        "}\n" + "\n";
    var url = 'https://graphql.anilist.co',
        options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                query: query,
                variables: my_variables
            })
        };

    fetch(url, options).then(response => response.json())
        .then(data => create_element_to_display(data));
}

    document.getElementById("search-form").addEventListener('submit', function (e) {
        let username = document.getElementById("user-name").value;
        if (username.trim().length === 0)
            return;

        e.preventDefault();

        let startDate = document.getElementById('started-date').value;
        let finishedDate = document.getElementById('finished-date').value;

        let allPreview = document.getElementById('preview-search');
        let awaitDoc = document.createElement("img");
        awaitDoc.src = "img/loading.gif";
        awaitDoc.className = "await-center";
        allPreview.appendChild(awaitDoc);
        fetch_search(username, startDate.replaceAll('-', ''), finishedDate.replaceAll('-', ''));
    })
