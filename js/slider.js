const formatAnime = ["Movie", "Music", "Special", "ONA", "OVA", "TV Short", "TV Show"];
const formatManga = ["Manga", "Light Novel", "One Shot"];
const stateManga = ["Reading", "Plan to read", "Completed", "Rereading", "Paused", "Dropped"];
const stateAnime = ["Completed", "Watching", "Rewatching", "Planned", "Dropped", "Paused"]

function listChange(list, listName) {
    const listFormat = document.getElementById(listName);
    const listFormatChildren = listFormat.children;
    for (let i = 0; i < listFormatChildren.length; i++) {
        const ids = listFormatChildren[i].id.split("-");
        const type = ids[ids.length - 1];
        listFormatChildren[i].hidden = !list.includes(type);
    }
}

function changeDisplay() {
    const seasonDiv = document.getElementById("season-div");
    
    seasonDiv.hidden = !seasonDiv.hidden;
}


document.getElementById("switch-type").addEventListener("click", (e) => {
    const label = document.getElementById("anime-manga-label");
    const checker = document.getElementById("checked-slider");
    const tagList = document.getElementById("tag-list-all");

    if (label.innerHTML === "Manga") {
        label.innerHTML = "Anime";
        listChange(formatAnime, "listFormat");
        listChange(stateAnime, "listState");
        changeDisplay();
    } else {
        listChange(formatManga, "listFormat");
        listChange(stateManga, "listState");
        label.innerHTML = "Manga";
        changeDisplay();
    }
    checker.checked = !checker.checked;
    while (tagList.children.length >= 2) {
        const ids = tagList.children[1].id.split("-");
        const type = ids[ids.length - 1];
        uncheck_tag(tagList.children[1].id, type, ids[0] + "-" + ids[1]);
    }
    e.preventDefault();
});

window.onload =function () {
    listChange(formatAnime, "listFormat");
    listChange(stateAnime, "listState");
};