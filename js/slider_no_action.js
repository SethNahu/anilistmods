document.getElementById("switch-type").addEventListener("click", (e) => {
    const label = document.getElementById("anime-manga-label");
    const checker = document.getElementById("checked-slider");

    if (label.innerHTML === "Manga")
        label.innerHTML = "Anime";
    else
        label.innerHTML = "Manga";
    checker.checked = !checker.checked;
    get_user_information();
    e.preventDefault();
});